﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Acquisition" Type="Folder">
			<Item Name="Acquisition.vi" Type="VI" URL="../Acquisition.vi"/>
			<Item Name="Acquisition_state.ctl" Type="VI" URL="../Acquisition_state.ctl"/>
			<Item Name="Sending_state.ctl" Type="VI" URL="../Sending_state.ctl"/>
		</Item>
		<Item Name="Analysis" Type="Folder">
			<Item Name="Analysis.vi" Type="VI" URL="../Analysis.vi"/>
			<Item Name="Analysis_Command.ctl" Type="VI" URL="../Analysis_Command.ctl"/>
			<Item Name="Analysis_State.ctl" Type="VI" URL="../Analysis_State.ctl"/>
		</Item>
		<Item Name="Controller" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Controller.vi" Type="VI" URL="../Controller.vi"/>
			<Item Name="algorithm_data.ctl" Type="VI" URL="../algorithm_data.ctl"/>
			<Item Name="algorithm_data_array.ctl" Type="VI" URL="../algorithm_data_array.ctl"/>
			<Item Name="Controller_Command.ctl" Type="VI" URL="../Controller_Command.ctl"/>
			<Item Name="Controller_State.ctl" Type="VI" URL="../Controller_State.ctl"/>
			<Item Name="Queue_References.ctl" Type="VI" URL="../Queue_References.ctl"/>
			<Item Name="Data_append.vi" Type="VI" URL="../Data_append.vi"/>
		</Item>
		<Item Name="Database" Type="Folder">
			<Item Name="Database.vi" Type="VI" URL="../Database.vi"/>
			<Item Name="Db_Command.ctl" Type="VI" URL="../Db_Command.ctl"/>
			<Item Name="Db_state.ctl" Type="VI" URL="../Db_state.ctl"/>
		</Item>
		<Item Name="GUI" Type="Folder">
			<Item Name="GUI.vi" Type="VI" URL="../GUI.vi"/>
			<Item Name="Gui_Command.ctl" Type="VI" URL="../Gui_Command.ctl"/>
			<Item Name="Gui_state.ctl" Type="VI" URL="../Gui_state.ctl"/>
		</Item>
		<Item Name="Main.vi" Type="VI" URL="../Main.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="VISA Flush IO Buffer Mask.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Flush IO Buffer Mask.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
